import { gql } from 'apollo-server-express';

const Language = gql`
  extend type Query {
    language(id: ID!): Language
    allLanguages(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: LanguageFilter): [Language]
    _allLanguagesMeta(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: LanguageFilter): ListMetadata
  }

  extend type Mutation {
    createLanguage(input: LanguageInput!): Language
    addLevel(id: ID!, levelId: ID!, order: Int): Language
  }

  type Language {
    id: ID!
    title: String!
    description: String
    activated: Boolean
    levels: [LevelType]
  }

  type LevelType {
    id: Level!,
    order: Int
  }

  input LanguageInput {
    title: String!
    description: String
  }

  input LanguageFilter {
    q: String
    id: ID
    title: String
    activated: Boolean
  }
`;

export default Language;