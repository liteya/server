export const Query = {
  language: (_, { id }, context) => {
    return context.languageRepository.findLanguage({ _id : id });
  },
  allLanguages: (_, {page, perPage, sortField, sortOrder, filter}, context) => {
    return context.languageRepository.getLanguages(page*perPage, perPage, sortField, sortOrder, filter);
  },
  _allLanguagesMeta: (_, {page, perPage, sortField, sortOrder, filter}, context) => {
    return { count: context.languageRepository.countLanguages(0, 0, sortField, sortOrder, filter) };
  },
};

export const Mutation = {
  createLanguage: async (_, { input }, context) => {
    return context.languageRepository.createLanguage(input);
  },
  addLevel: async (_, { id, levelId, order }, context) => {
    return context.languageRepository.addLevel(id, levelId, order);
  }
};

export const Language = {};
