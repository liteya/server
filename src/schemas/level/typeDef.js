import { gql } from 'apollo-server-express';

const Level = gql`
  extend type Query {
    Level(id: ID!): Level
    allLevels(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: LevelFilter): [Level]
    _allLevelsMeta(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: LevelFilter): ListMetadata
  }

  extend type Mutation {
    createLevel(input: LevelInput!): Level
    addChapter(id: ID!, chapterId: ID!, order: Int): Level
  }

  type Level {
    id: ID
    title: String!
    description: String
    activated: Boolean
    language: Language
    chapters: [ChapterType]
  }

  type ChapterType {
    id: Chapter!,
    order: Int
  }

  input LevelInput {
    title: String!
    description: String
  }

  input LevelFilter {
    q: String
    id: ID
    title: String
  }
`;

export default Level;