export const Query = {
  Level: (_, { id }, context) => {
    return context.levelRepository.findLevel({ _id : id });
  },
  allLevels: (_, {page, perPage, sortField, sortOrder, filter}, context) => {
    return context.levelRepository.getLevels(page*perPage, perPage, sortField, sortOrder, filter);
  },
  _allLevelsMeta: (_, {page, perPage, sortField, sortOrder, filter}, context) => {
    return { count: context.levelRepository.countLevels(0, 0, sortField, sortOrder, filter) };
  },
};

export const Mutation = {
  createLevel: async (_, { input }, context) => {
    return context.levelRepository.createLevel(input);
  },
  addChapter: async (_, { id, chapterId, order }, context) => {
    return context.levelRepository.addChapter(id, chapterId, order);
  }
};

export const Level = {};
