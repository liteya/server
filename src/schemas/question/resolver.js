import { uploadFile } from '../../utils/AWSS3Uploader';

export const Query = {
  Question: (_, { id }, context) => {
    return context.questionRepository.findQuestion({ _id : id });
  },
  allQuestions: (_, {page, perPage, sortField, sortOrder, filter}, context) => {
    return context.questionRepository.getQuestions(page*perPage, perPage, sortField, sortOrder, filter);
  },
  _allQuestionsMeta: (_, {page, perPage, sortField, sortOrder, filter}, context) => {
    return { count: context.questionRepository.countQuestions(0, 0, sortField, sortOrder, filter) };
  },
};

export const Mutation = {
  createQuestion: async (_, { title, type, content, courseId, activated }, context) => {

    if (['LearnExpression', 'TranslateExpressionByWriting', 'TranslateWriteExpressionWithBlock', 'ChooseRightAnswer', 'Dialog'].includes(type)) {
      let imageResult;
      if (content && content.expression && content.expression.image && content.expression.image.rawFile && content.expression.image.rawFile instanceof Promise) {
        const { createReadStream, filename, mimetype } = await content.expression.image.rawFile;
        imageResult = await uploadFile(createReadStream(), filename, mimetype, 'image');
        content.expression.image.url = imageResult.Location;
      }

      let audioResult;
      if (content && content.expression && content.expression.audio && content.expression.audio.rawFile && content.expression.audio.rawFile instanceof Promise) {
        const { createReadStream, filename, mimetype } = await content.expression.audio.rawFile;
        audioResult = await uploadFile(createReadStream(), filename, mimetype, 'audio');
        content.expression.audio.url = audioResult.Location;
      }
    }

    if (type === 'ChooseRightAnswer') {
      if (content && content.possible_answers) {
        for (let i = 0; i < content.possible_answers.length; i++) {
          let imageResult;
          if (content.possible_answers[i].image && content.possible_answers[i].image.rawFile && content.possible_answers[i].image.rawFile instanceof Promise) {
            const { createReadStream, filename, mimetype } = await content.possible_answers[i].image.rawFile;
            imageResult = await uploadFile(createReadStream(), filename, mimetype, 'audio');
            content.possible_answers[i].image.url = imageResult.Location;
          }

          let audioResult;
          if (content.possible_answers[i].audio && content.possible_answers[i].audio.rawFile && content.possible_answers[i].audio.rawFile instanceof Promise) {
            const { createReadStream, filename, mimetype } = await content.possible_answers[i].audio.rawFile;
            audioResult = await uploadFile(createReadStream(), filename, mimetype, 'audio');
            content.possible_answers[i].audio.url = audioResult.Location;
          }
        }
      }
    }
    
    return context.questionRepository.createQuestion({ title, type, content, courseId, activated });
  },
  updateQuestion: async (_, { id, title, type, content, activated }, context) => {

    if (['LearnExpression', 'TranslateExpressionByWriting', 'TranslateWriteExpressionWithBlock', 'ChooseRightAnswer', 'Dialog'].includes(type)) {
      let imageResult;
      if (content && content.expression && content.expression.image && content.expression.image.rawFile && content.expression.image.rawFile instanceof Promise) {
        const { createReadStream, filename, mimetype } = await content.expression.image.rawFile;
        imageResult = await uploadFile(createReadStream(), filename, mimetype, 'image');
        content.expression.image.url = imageResult.Location;
      }

      let audioResult;
      if (content && content.expression && content.expression.audio && content.expression.audio.rawFile && content.expression.audio.rawFile instanceof Promise) {
        const { createReadStream, filename, mimetype } = await content.expression.audio.rawFile;
        audioResult = await uploadFile(createReadStream(), filename, mimetype, 'audio');
        content.expression.audio.url = audioResult.Location;
      }
    }

    if (type === 'ChooseRightAnswer') {
      if (content && content.possible_answers) {
        for (let i = 0; i < content.possible_answers.length; i++) {
          let imageResult;
          if (content.possible_answers[i].image && content.possible_answers[i].image.rawFile && content.possible_answers[i].image.rawFile instanceof Promise) {
            const { createReadStream, filename, mimetype } = await content.possible_answers[i].image.rawFile;
            imageResult = await uploadFile(createReadStream(), filename, mimetype, 'audio');
            content.possible_answers[i].image.url = imageResult.Location;
          }

          let audioResult;
          if (content.possible_answers[i].audio && content.possible_answers[i].audio.rawFile && content.possible_answers[i].audio.rawFile instanceof Promise) {
            const { createReadStream, filename, mimetype } = await content.possible_answers[i].audio.rawFile;
            audioResult = await uploadFile(createReadStream(), filename, mimetype, 'audio');
            content.possible_answers[i].audio.url = audioResult.Location;
          }
        }
      }
    }

    return context.questionRepository.updateQuestion(id, { title, type, content, activated } );
  },
};

export const Question = {};
