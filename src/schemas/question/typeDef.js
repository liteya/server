import { gql } from 'apollo-server-express';
import GraphQLJSON from 'graphql-type-json';

const Question = gql`
  extend type Query {
    Question(id: ID!): Question
    allQuestions(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: QuestionFilter): [Question]
    _allQuestionsMeta(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: QuestionFilter): ListMetadata
  }

  extend type Mutation {
    createQuestion(
      title: String!
      type: String!
      content: JSON
      activated: Boolean
      courseId: ID
    ): Question
    updateQuestion(
      id: ID!
      title: String
      type: String
      content: JSON
      activated: Boolean
    ): Question
  }

  type Question {
    id: ID!
    type: String!
    title: String
    content: JSON
    activated: Boolean
    course: Course
  }

  input QuestionFilter {
    q: String
    id: ID
    course: ID
  }

  scalar JSON
`;

export default Question;