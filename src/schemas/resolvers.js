import { Query as UserQuery, Mutation as UserMutation } from './user/resolver';
import { Query as LanguageQuery, Mutation as LanguageMutation } from './language/resolver';
import { Query as LevelQuery, Mutation as LevelMutation } from './level/resolver';
import { Query as ChapterQuery, Mutation as ChapterMutation } from './chapter/resolver';
import { Query as CourseQuery, Mutation as CourseMutation } from './course/resolver';
import { Query as QuestionQuery, Mutation as QuestionMutation } from './question/resolver';

const resolvers = {
  Query: {
    ...UserQuery,
    ...LanguageQuery,
    ...LevelQuery,
    ...ChapterQuery,
    ...CourseQuery,
    ...QuestionQuery,
  },
  Mutation: {
    ...UserMutation,
    ...LanguageMutation,
    ...LevelMutation,
    ...ChapterMutation,
    ...CourseMutation,
    ...QuestionMutation,
  }
};

export default resolvers;
