//import checkChapterAuth from '../../logic/authentication'
import { uploadFile } from '../../utils/AWSS3Uploader';
import fs from 'fs';

export const Query = {
  Chapter: async (_, { id }, context) => {
    return context.chapterRepository.findChapter({ _id : id });
  },
  allChapters: (_, {page, perPage, sortField, sortOrder, filter}, context) => {
    return context.chapterRepository.getChapters(page*perPage, perPage, sortField, sortOrder, filter);
  },
  _allChaptersMeta: (_, {page, perPage, sortField, sortOrder, filter}, context) => {
    return { count: context.chapterRepository.countChapters(0, 0, sortField, sortOrder, filter) };
  },
};

export const Mutation = {
  createChapter: async (_, { title, description, image, levelId, activated, premiumAccess }, context) => {
    //checkChapterAuth(context);
    let result;
    if (image) {

      const { createReadStream, filename, mimetype } = await image.rawFile;

      result = await uploadFile(createReadStream(), filename, mimetype);
    }

    let input = {
      title,
      description,
      levelId,
      activated,
      premiumAccess 
    };

    if (result !== undefined) {
      image.url = result.Location;
      input = {
        ...input,
        image
      }
    };

    const chapter = context.chapterRepository.createChapter(input);

    return chapter;
  },
  updateChapter: async (_, { id, title, description, activated, image, premiumAccess }, context) => {
    //checkChapterAuth(context);
    let result;
    if (image && image.rawFile && image.rawFile instanceof Promise) {
      const { createReadStream, filename, mimetype } = await image.rawFile;

      result = await uploadFile(createReadStream(), filename, mimetype);
    }

    let input = {
      title,
      description,
      activated,
      premiumAccess  
    };

    if (result !== undefined) {
      image.url = result.Location;
      input = {
        ...input,
        image
      }
    };

    const chapter = context.chapterRepository.updateChapter(id, input);

    return chapter;
  },
  addCourse: async (_, { id, courseId, order }, context) => {
    return context.chapterRepository.addCourse(id, courseId, order);
  }
};

export const Chapter = {};
