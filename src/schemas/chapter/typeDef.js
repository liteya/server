import { gql } from 'apollo-server-express';

const Chapter = gql`
  extend type Query {
    Chapter(id: ID!): Chapter
    allChapters(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: ChapterFilter): [Chapter]
    _allChaptersMeta(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: ChapterFilter): ListMetadata
  }

  extend type Mutation {
    createChapter(
      title: String!
      description: String
      image: JSON
      activated: Boolean
      premiumAccess: Boolean
      levelId: ID
    ): Chapter
    updateChapter(
      id: ID!
      title: String
      description: String
      activated: Boolean
      premiumAccess: Boolean
      image: JSON
    ): Chapter
    addCourse(id: ID!, courseId: ID!, order: Int): Chapter
  }

  type Chapter {
    id: ID
    title: String!
    description: String
    image: JSON
    activated: Boolean!
    premiumAccess: Boolean
    level: Level
    courses: [CourseType]
  }

  type CourseType {
    id: Course!,
    order: Int
  }

  input ChapterFilter {
    q: String
    id: ID
    level: ID
  }

`;

export default Chapter;