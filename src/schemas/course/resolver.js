export const Query = {
  Course: (_, { id }, context) => {
    return context.courseRepository.findCourse({ _id : id });
  },
  allCourses: (_, {page, perPage, sortField, sortOrder, filter}, context) => {
    return context.courseRepository.getCourses(page*perPage, perPage, sortField, sortOrder, filter);
  },
  _allCoursesMeta: (_, {page, perPage, sortField, sortOrder, filter}, context) => {
    return { count: context.courseRepository.countCourses(0, 0, sortField, sortOrder, filter) };
  },
};

export const Mutation = {
  createCourse: async (_, { title, description, chapterId, activated }, context) => {
    return context.courseRepository.createCourse({ title, description, chapterId, activated });
  },
  updateCourse: async (_, { id, title, description, activated }, context) => {
    return context.courseRepository.updateCourse(id, { title, description, activated } );
  },
};

export const Course = {};
