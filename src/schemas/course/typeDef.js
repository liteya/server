import { gql } from 'apollo-server-express';

const Course = gql`
  extend type Query {
    Course(id: ID!): Course
    allCourses(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: CourseFilter): [Course]
    _allCoursesMeta(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: CourseFilter): ListMetadata
  }

  extend type Mutation {
    createCourse(
      title: String!
      description: String
      activated: Boolean
      chapterId: ID
    ): Course,
    updateCourse(
      id: ID!
      title: String
      description: String
      activated: Boolean
    ): Course
  }

  type Course {
    id: ID
    title: String!
    description: String
    activated: Boolean
    questions: [QuestionType]
    chapter: Chapter
  }

  type QuestionType {
    id: Question!,
    order: Int
  }

  input CourseFilter {
    q: String
    id: ID
    chapter: ID
  }
`;

export default Course;