import User from './user/typeDef';
import Language from './language/typeDef';
import Question from './question/typeDef';
import Level from './level/typeDef';
import Chapter from './chapter/typeDef';
import Course from './course/typeDef';

const typeDefs = [User, Language, Level, Chapter, Course, Question];

export default typeDefs;
