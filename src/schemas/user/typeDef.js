import { gql } from 'apollo-server-express';

const User = gql`
  type Query {
    User(id: ID!): User
    allUsers(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: UserFilter): [User]
    _allUsersMeta(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: UserFilter): ListMetadata
  }

  type Mutation {
    signup(input: UserInput!): AuthPayload!
    login(email: String!, password: String!): AuthPayload!
    addUserLanguage(userId: ID!, languageId: ID!): User!
    updateUser(
      id: ID!
      name: String
      email: String
      password: String
    ): User!
    activateCurrentLearningLanguage(userId: ID!, languageId: ID!): User!
    addChapterCompleted(userId: ID!, chapterId: ID!, languageId: ID!): User!
    addCourseCompleted(userId: ID!, courseId: ID!, chapterId: ID!): User!
    handleUserSubscriptionEvent(content: JSON!): JSON
  }

  type User {
    id: ID
    email: String!
    name: String
    premiumAccess: UserPremiumAccess
    languages: [UserLanguage]
    createdAt: Date
  }
  
  type UserPremiumAccess {
    activated: Boolean
    formula: String
  }

  type UserLanguage {
    id: Language!
    isCurrentLearning: Boolean!
    levels: [UserLevel]
  }

  type UserLevel {
    id: Level!
    chapters: UserChapter
  }

  type UserChapter {
    completed: [Chapter]
    current: UserChapterCurrent
  }

  type UserChapterCompleted {
    id: Chapter
  }

  type UserChapterCurrent {
    id: Chapter
    courses: UserChapterCurrentCourses
  }

  type UserChapterCurrentCourses {
    completed: [Course]
  }

  type AuthPayload {
    token: String!
    user: User!
  }

  input UserInput {
    email: String!
    password: String!
  }

  input UserFilter {
    q: String
    id: ID
    email: String
  }

  type ListMetadata {
    count: Int!
  }
  
  scalar Date
`;

export default User;