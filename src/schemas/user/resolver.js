import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
//import checkUserAuth from '../../logic/authentication';

dotenv.config();

export const Query = {
  User: (_, { id }, context) => {
    //checkUserAuth(context);
    return context.userRepository.findUser({ _id : id });
  },
  allUsers: (_, {page, perPage, sortField, sortOrder, filter}, context) => {
    //checkUserAuth(context);
    return context.userRepository.getUsers(page*perPage, perPage, sortField, sortOrder, filter);
  },
  _allUsersMeta: (_, {page, perPage, sortField, sortOrder, filter}, context) => {
    //checkUserAuth(context);
    return { count: context.userRepository.countUsers(0, 0, sortField, sortOrder, filter) };
  },
};

export const Mutation = {
  signup: async (_, { input }, context) => {
    //checkUserAuth(context);
    const userFounded = await context.userRepository.findUser({ email: input.email });
  
    if (userFounded) {
      throw new Error("Un utilisateur existe déjà avec cette adresse email");
    }

    const user = context.userRepository.createUser(input);

    const token = jwt.sign({ id: user.id, email: user.email }, process.env.ACCESS_TOKEN_SECRET, {
      expiresIn: process.env.ACCESS_TOKEN_LIFE
    });

    return {
      token,
      user
    };
  },
  login: async (_, { email, password }, context) => {
    const user = await context.userRepository.findUser({ email });
  
    if (!user) {
      throw new Error("Aucun utilisateur n'a été trouvé avec cette adresse email");
    }

    const isPasswordMatch = await user.comparePassword(password);
    if (!isPasswordMatch) {
        throw new Error('Mot de passe invalide');
    }

    const token = jwt.sign({ id: user.id, email: user.email }, process.env.ACCESS_TOKEN_SECRET, {
      expiresIn: process.env.ACCESS_TOKEN_LIFE
    });

    return {
      token,
      user
    };
  },
  addUserLanguage: async (_, { userId, languageId }, context) => {
    const user = await context.userRepository.addUserLanguage({ userId, languageId });
    
    return user;
  },
  updateUser: async (_, { id, name, email, password }, context) => {
    return context.userRepository.updateUser(id, { name, email, password } );
  },
  activateCurrentLearningLanguage: async (_, { userId, languageId }, context) => {
    const user = await context.userRepository.activateCurrentLearningLanguage({ userId, languageId });
    
    return user;
  },
  addChapterCompleted: async (_, { userId, chapterId, languageId }, context) => {
    const user = await context.userRepository.addChapterCompleted({ userId, chapterId, languageId });
    
    return user;
  },
  addCourseCompleted: async (_, { userId, courseId, chapterId }, context) => {
    const user = await context.userRepository.addCourseCompleted({ userId, courseId, chapterId });
    
    return user;
  },
  handleUserSubscriptionEvent: async (_, { content }, context) => {
    console.info('------ handleUserSubscriptionEvent --------- ');

    const bodyContent = JSON.parse(content.body);

    const userId = bodyContent.data.userId;
    const eventType = bodyContent.type.trim();

    console.info('Content: ', bodyContent);
    console.info('Event to process:', eventType);

    if (['purchase', 'subscription_renewal'].includes(eventType)) {
      await context.userRepository.activatedPremiumAccess(userId);
      console.info('Event processed: ', eventType);
    } else if (['subscription_pause', 'subscription_expire'].includes(eventType)) {
      await context.userRepository.deactivatedPremiumAccess(userId);
      console.info('Event processed: ', eventType);
    } else {
      console.warn(`Event "${eventType}" for user "${userId}" are not need to be handle`);
    }
  },
};

export const User = {};
