import userRepository from './userRepository';
import languageRepository from './languageRepository';
import levelRepository from './levelRepository';
import chapterRepository from './chapterRepository';
import courseRepository from './courseRepository';
import questionRepository from './questionRepository';

export {
  userRepository,
  languageRepository,
  levelRepository,
  chapterRepository,
  courseRepository,
  questionRepository
}
