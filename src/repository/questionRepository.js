import Question from '../models/question';
import Course from '../models/course';
import courseRepository from './courseRepository';

const createQuestion = async (input) => {
  let question = new Question({
    title: input.title,
    type: input.type,
    content: input.content,
    activated: input.activated
  });

  try {
    question = await question.save();
  
    if (input.courseId) {
      console.log('courseId ', input.courseId);
      const course = await courseRepository.addQuestion(input.courseId, question.id, null);
      question.course = course;
    }

    return question;

  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateQuestion = async (id, { title, type, content, courseId, activated }) => {
  const question = await Question
    .findById(id)
    .populate({
      path: 'course',
      model: Course,
    });

  if (!question) {
    throw new Error(`Impossible de trouver la question avec id: ${id}`);
  }

  if (title !== undefined) {
    question.title = title;
  }

  if (type !== undefined) {
    question.type = type;
  }

  if (content !== undefined) {
    question.content = content;
  }

  if (activated !== undefined) {
    question.activated = activated;
  }

  if (courseId !== undefined) {
    question.course = courseId;
  }

  try {
    return await question.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const getQuestions = async (skip, limit, sortField, sortOrder, filter) => {
  try {
    return await Question.find(filter)
      .sort({ [sortField]: sortOrder })
      .skip(skip)
      .limit(limit)
      .populate({
        path: 'course',
        model: Course,
      });
  } catch (err) {
    console.error(err);
    return err;
  }
};

const countQuestions = async (skip, limit, sortField, sortOrder, filter) => {
  try {
    return await Question.find(filter)
      .sort({ [sortField]: sortOrder })
      .skip(skip)
      .limit(limit)
      .estimatedDocumentCount();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const findQuestion = async filters => {
  try {
    return await Question
      .findOne(filters)
      .populate({
        path: 'course',
        model: Course,
      });
  } catch (err) {
    return err;
  }
};

export default {
  createQuestion,
  getQuestions,
  countQuestions,
  findQuestion,
  updateQuestion
};
