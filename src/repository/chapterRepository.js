import Chapter from '../models/chapter';
import Course from '../models/course';
import Level from '../models/level';
import Question from '../models/question';
import courseRepository from './courseRepository';
import levelRepository from './levelRepository';

const createChapter = async (input) => {
  let chapter = new Chapter({
    title: input.title,
    description: input.description,
    activated: input.activated,
    premiumAccess: input.premiumAccess,
    image: input.image !== undefined ? input.image : null,
  });

  try {
    chapter = await chapter.save();
    if (input.levelId) {
      const level = levelRepository.addChapter(input.levelId, chapter.id, null);

      chapter.level = level;
    }

    return chapter;
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateChapter = async (id, { title, description, activated, image, levelId, premiumAccess }) => {
  const chapter = await Chapter
    .findById(id)
    .populate({
      path: 'courses.id',
      model: Course,
    })
    .populate({
      path: 'level',
      model: Level,
    });

  if (!chapter) {
    throw new Error(`Impossible de trouver le chapitre avec id: ${id}`);
  }

  if (title !== undefined) {
    chapter.title = title;
  }

  if (description !== undefined) {
    chapter.description = description;
  }

  if (activated !== undefined) {
    chapter.activated = activated;
  }

  if (image !== undefined) {
    chapter.image = image;
  }

  if (levelId !== undefined) {
    chapter.level = levelId;
  }

  if (premiumAccess !== undefined) {
    chapter.premiumAccess = premiumAccess;
  }

  try {
    return await chapter.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const getChapters = async (skip, limit, sortField, sortOrder, filter) => {
  try {
    return await Chapter.find(filter)
      .sort({ [sortField]: sortOrder })
      .skip(skip)
      .limit(limit)
      .populate({
        path: 'courses.id',
        model: Course,
        populate: {
          path: 'questions.id',
          model: Question,
        },
      })
      .populate({
        path: 'level',
        model: Level,
      });
  } catch (err) {
    console.error(err);
    return err;
  }
};

const countChapters = async (skip, limit, sortField, sortOrder, filter) => {
  try {
    return await Chapter.find(filter)
      .sort({ [sortField]: sortOrder })
      .skip(skip)
      .limit(limit)
      .estimatedDocumentCount();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const findChapter = async filter => {
  try {
    return await Chapter
      .findOne(filter)
      .populate({
        path: 'courses.id',
        model: Course,
      })
      .populate({
        path: 'level',
        model: Level,
      });
  } catch (err) {
    return err;
  }
};

const addCourse = async (id, courseId, order) => {
  const chapter = await Chapter.findById(id);

  if (!chapter) {
    throw new Error(`Impossible de trouver la rubrique #${id}`);
  }

  let { courses } = chapter;

  if (!courses) {
    courses = [];
  }

  const questionOrder = order ? order : courses.length + 1;

  courses.push({ id: courseId, order: questionOrder });

  chapter.courses = courses;

  try {
    courseRepository.updateCourse(courseId, { chapterId : chapter.id });
    return await chapter.save();
  } catch (err) {
    console.error(err);
    return err;
  }

};

export default {
  createChapter,
  getChapters,
  countChapters,
  findChapter,
  updateChapter,
  addCourse
};
