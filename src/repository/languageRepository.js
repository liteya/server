import Language from '../models/language';
import Level from '../models/level';
import Chapter from '../models/chapter';
import Course from '../models/course';
import Question from '../models/question';

const createLanguage = async (input) => {
  const language = new Language({
    title: input.title,
    description: input.description,
  });

  try {
    return await language.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const getLanguages = async (skip, limit, sortField, sortOrder, filter) => {
  try {
    return await Language.find(filter)
      .sort({ [sortField]: sortOrder })
      .skip(skip)
      .limit(limit)
      .populate({
        path: 'levels.id',
        model: Level,
        populate: {
          path: 'chapters.id',
          model: Chapter,
          populate: {
            path: 'courses.id',
            model: Course,
          },
        },
      });

  } catch (err) {
    console.error(err);
    return err;
  }
};

const countLanguages = async (skip, limit, sortField, sortOrder, filter) => {
  try {
    return await Language.find(filter)
      .sort({ [sortField]: sortOrder })
      .skip(skip)
      .limit(limit)
      .estimatedDocumentCount();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const findLanguage = async filters => {
  try {
    return await Language
      .findOne(filters)
      .populate({
        path: 'levels.id',
        model: Level,
        populate: {
          path: 'chapters.id',
          model: Chapter,
          populate: {
            path: 'courses.id',
            model: Course,
            populate: {
              path: 'questions.id',
              model: Question,
            }
          },
        },
      });
  } catch (err) {
    return err;
  }
};

const findLanguageById = async id => {
  try {
    return await Language
      .findById(id)
      .populate({
        path: 'levels.id',
        model: Level,
      });
  } catch (err) {
    return err;
  }
};

// todo: set level also
const addLevel = async (id, levelId, order) => {
  const language = await Language.findById(id);

  if (!language) {
    throw new Error(`Impossible de trouver la langue #${id}`);
  }

  let { levels } = language;

  if (!levels) {
    levels = [];
  }

  levels.push({ id: levelId, order });

  language.levels = levels;

  try {
    return await language.save();
  } catch (err) {
    console.error(err);
    return err;
  }

};

export default {
  createLanguage,
  getLanguages,
  countLanguages,
  findLanguage,
  findLanguageById,
  addLevel
};
