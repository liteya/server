import User from '../models/user';
import Language from '../models/language';
import Level from '../models/level';
import Chapter from '../models/chapter';
import Course from '../models/course';
import languageRepository from './languageRepository';
import chapterRepository from './chapterRepository';
import { async } from 'regenerator-runtime';

const newUser = (inputData) => {
  const user = new User({
    email: inputData.email,
    password: inputData.password,
    name: inputData.name
});

  return user;
};

const createUser = async (inputData) => {
  const user = newUser(inputData);

  try {
    return await user.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateUser = async (id, inputData) => {
  console.log('Update user ', inputData);
  const user = await User
    .findById(id)
    .populate({
      path: 'languages.id',
      model: Language,
    })
    .populate({
      path: 'languages.levels.id',
      model: Level,
    })
    .populate({
      path: 'languages.levels.chapters.completed',
      model: Chapter,
    })
    .populate({
      path: 'languages.levels.chapters.current.id',
      model: Chapter,
    })
    .populate({
      path: 'languages.levels.chapters.current.courses.completed',
      model: Course,
    });
    
  if (!user) {
    throw new Error(`Impossible de trouver le client avec id: ${inputData.id}`);
  }

  if (inputData.name) {
    user.name = inputData.name;
  }

  if (inputData.email) {
    user.email = inputData.email;
  }

  if (inputData.password) {
    user.password = inputData.password;
  }

  if (inputData.premiumAccess) {
    user.premiumAccess = inputData.premiumAccess;
  }

  try {
    return await user.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const addUserLanguage = async ({ userId, languageId }) => {
  const user = await User
    .findById(userId)
    .populate({
      path: 'languages.id',
      model: Language,
    })
    .populate({
      path: 'languages.levels.id',
      model: Level,
    })
    .populate({
      path: 'languages.levels.chapters.completed',
      model: Chapter,
    })
    .populate({
      path: 'languages.levels.chapters.current.id',
      model: Chapter,
    })
    .populate({
      path: 'languages.levels.chapters.current.courses.completed',
      model: Course,
    });

  if (!user) {
    throw new Error(`Impossible de trouver le client avec id: ${inputData.id}`);
  }

  if (user.languages) {
    const languageIndex = user.languages.findIndex((elt) => String(elt.id._id) === languageId);
    if (languageIndex !== -1) {
      console.warn(`L'utilisateur #${userId} a déjà la langue #${languageId}`)
      return user;
    }
  }

  deactivateAllUserLanguages(user);

  const language = await languageRepository.findLanguageById(languageId)
  const firstLevelId = language.levels[0].id._id;
  let userLanguages = user.languages ? user.languages : [];
  let userLanguageLevels = [];

  const userLanguageLevel = {
    id: firstLevelId
  };

  userLanguageLevels.push(userLanguageLevel);

  const userLanguage = {
    id: languageId,
    isCurrentLearning: true,
    levels: userLanguageLevels
  };

  userLanguages.push(userLanguage);

  user.languages = userLanguages;

  try {
    await user.save();

    return await User
      .findById(userId)
      .populate({
        path: 'languages.id',
        model: Language,
      })
      .populate({
        path: 'languages.levels.id',
        model: Level,
      })
      .populate({
        path: 'languages.levels.chapters.completed',
        model: Chapter,
      })
      .populate({
        path: 'languages.levels.chapters.current.id',
        model: Chapter,
      })
      .populate({
        path: 'languages.levels.chapters.current.courses.completed',
        model: Course,
      });
      
  } catch (err) {
    console.error(err);
    return err;
  }
};

const activateCurrentLearningLanguage = async ({ userId, languageId }) => {
  const user = await User
    .findById(userId)
    .populate({
      path: 'languages.id',
      model: Language,
    })
    .populate({
      path: 'languages.levels.id',
      model: Level,
    })
    .populate({
      path: 'languages.levels.chapters.completed',
      model: Chapter,
    })
    .populate({
      path: 'languages.levels.chapters.current.id',
      model: Chapter,
    })
    .populate({
      path: 'languages.levels.chapters.current.courses.completed',
      model: Course,
    });

  if (!user) {
    throw new Error(`Impossible de trouver le client avec id: ${userId}`);
  }

  deactivateAllUserLanguages(user);

  let userLanguages = user.languages;
  const languageIndex = userLanguages.findIndex((elt) => String(elt.id._id) === languageId);
  let userLanguageToActivate = userLanguages[languageIndex];

  userLanguageToActivate.isCurrentLearning = true;

  userLanguages.splice(languageIndex, 1, userLanguageToActivate);

  user.languages = userLanguages;

  try {
    await user.save();

    return await User
      .findById(userId)
      .populate({
        path: 'languages.id',
        model: Language,
      })
      .populate({
        path: 'languages.levels.id',
        model: Level,
      })
      .populate({
        path: 'languages.levels.chapters.completed',
        model: Chapter,
      })
      .populate({
        path: 'languages.levels.chapters.current.id',
        model: Chapter,
      })
      .populate({
        path: 'languages.levels.chapters.current.courses.completed',
        model: Course,
      });

  } catch (err) {
    console.error(err);
    return err;
  }
};

const addChapterCompleted = async ({ userId, chapterId }) => {
  const user = await User
    .findById(userId)
    .populate({
      path: 'languages.id',
      model: Language,
    })
    .populate({
      path: 'languages.levels.id',
      model: Level,
    });

  if (!user) {
    throw new Error(`Impossible de trouver le client avec id: ${userId}`);
  }

  const userLanguages = user.languages;
  const language = userLanguages.find((elt) => elt.isCurrentLearning);

  if (!language.levels[0].chapters.completed.includes(chapterId)) {
    language.levels[0].chapters.completed.push(chapterId);
    language.levels[0].chapters.current.id = null;
    language.levels[0].chapters.current.courses.completed = [];
  }

  user.languages = userLanguages;

  try {
    await user.save();

    return await User
      .findById(userId)
      .populate({
        path: 'languages.id',
        model: Language,
      })
      .populate({
        path: 'languages.levels.id',
        model: Level,
      })
      .populate({
        path: 'languages.levels.chapters.completed',
        model: Chapter,
      })
      .populate({
        path: 'languages.levels.chapters.current.id',
        model: Chapter,
      })
      .populate({
        path: 'languages.levels.chapters.current.courses.completed',
        model: Course,
      });

  } catch (err) {
    console.error(err);
    return err;
  }
};

const addCourseCompleted = async ({ userId, courseId, chapterId }) => {
  const user = await User
    .findById(userId)
    .populate({
      path: 'languages.id',
      model: Language,
    })
    .populate({
      path: 'languages.levels.id',
      model: Level,
    });

  if (!user) {
    throw new Error(`Impossible de trouver le client avec id: ${userId}`);
  }

  const userLanguages = user.languages;
  const language = userLanguages.find((elt) => elt.isCurrentLearning);

  if (!language.levels[0].chapters.completed.includes(chapterId)) {
  
    if (!language.levels[0].chapters.current.courses.completed.includes(courseId)) {
      
      if (!language.levels[0].chapters.current.id) {
        language.levels[0].chapters.current.id = chapterId;
      }
  
      language.levels[0].chapters.current.courses.completed.push(courseId);
      
      const chapter = await chapterRepository.findChapter({ _id : chapterId });

      if (chapter.courses.length === language.levels[0].chapters.current.courses.completed.length) {
        return await addChapterCompleted({ userId, chapterId });
      }

    }
  }


  user.languages = userLanguages;

  try {
    await user.save();

    return await User
      .findById(userId)
      .populate({
        path: 'languages.id',
        model: Language,
      })
      .populate({
        path: 'languages.levels.id',
        model: Level,
      })
      .populate({
        path: 'languages.levels.chapters.completed',
        model: Chapter,
      })
      .populate({
        path: 'languages.levels.chapters.current.id',
        model: Chapter,
      })
      .populate({
        path: 'languages.levels.chapters.current.courses.completed',
        model: Course,
      });

  } catch (err) {
    console.error(err);
    return err;
  }
};

const deactivateAllUserLanguages = (user) => {
  if (user.languages) {
    user.languages.forEach(function(language) {
      language.isCurrentLearning = false;
    }); 
  }
} ;

const getUsers = async (skip, limit, sortField, sortOrder, filter) => {
  try {
    return await User.find(filter)
      .populate({
        path: 'languages.id',
        model: Language,
        populate: {
          path: 'levels.id',
          model: Level,
        }
      })
      .populate({
        path: 'languages.levels.id',
        model: Level,
      })
      .populate({
        path: 'languages.levels.chapters.completed',
        model: Chapter,
      })
      .populate({
        path: 'languages.levels.chapters.current.id',
        model: Chapter,
      })
      .populate({
        path: 'languages.levels.chapters.current.courses.completed',
        model: Course,
      })
      .sort({ [sortField]: sortOrder })
      .skip(skip)
      .limit(limit);

  } catch (err) {
    console.error(err);
    return err;
  }
};

const countUsers = async (skip, limit, sortField, sortOrder, filter) => {
  try {
    return await User.find(filter)
      .sort({ [sortField]: sortOrder })
      .skip(skip)
      .limit(limit)
      .estimatedDocumentCount();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const findUser = async filters => {
  try {
    return await User
      .findOne(filters)
      .populate({
        path: 'languages.id',
        model: Language,
        populate: {
          path: 'levels.id',
          model: Level,
        }
      })
      .populate({
        path: 'languages.levels.id',
        model: Level,
      })
      .populate({
        path: 'languages.levels.chapters.completed',
        model: Chapter,
      })
      .populate({
        path: 'languages.levels.chapters.current.id',
        model: Chapter,
      })
      .populate({
        path: 'languages.levels.chapters.current.courses.completed',
        model: Course,
      });

  } catch (err) {
    return err;
  }
};

const activatedPremiumAccess = async (userId) => {
  const premiumAccess = {
    activated: true,
    formula: 'BASIC'
  };

  return await updateUser(userId, { premiumAccess } );
};

const deactivatedPremiumAccess = async (userId) => {
  const premiumAccess = {
    activated: false
  };

  return await updateUser(userId, { premiumAccess } );
};

export default {
  createUser,
  updateUser,
  getUsers,
  findUser,
  countUsers,
  addUserLanguage,
  activateCurrentLearningLanguage,
  addChapterCompleted,
  addCourseCompleted,
  activatedPremiumAccess,
  deactivatedPremiumAccess
};
