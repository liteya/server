import Course from '../models/course';
import Chapter from '../models/chapter';
import Question from '../models/question';
import chapterRepository from './chapterRepository';
import questionRepository from './questionRepository';

const createCourse = async (input) => {
  let course = new Course({
    title: input.title,
    description: input.description,
    activated: input.activated,
  });

  try {
    course = await course.save();
  
    if (input.chapterId) {
      const chapter = await chapterRepository.addCourse(input.chapterId, course.id, null);
      course.chapter = chapter;
    }

    return course;

  } catch (err) {
    console.error(err);
    return err;
  }
};

const updateCourse = async (id, input) => {
  const course = await Course
    .findById(id)
    .populate({
      path: 'questions.id',
      model: Question,
    })
    .populate({
      path: 'chapter',
      model: Chapter,
    });

  if (!course) {
    throw new Error(`Impossible de trouver la recette avec id: ${id}`);
  }

  if (input.title !== undefined) {
    course.title = input.title;
  }

  if (input.description !== undefined) {
    course.description = input.description;
  }

  if (input.activated !== undefined) {
    course.activated = input.activated;
  }

  if (input.chapterId !== undefined) {
    course.chapter = input.chapterId;
  }

  //if (input.questions !== undefined) {
    //course.questions = input.questions;
  //}

  try {
    return await course.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const getCourses = async (skip, limit, sortField, sortOrder, filter) => {
  try {
    return await Course.find(filter)
      .sort({ [sortField]: sortOrder })
      .skip(skip)
      .limit(limit)
      .populate({
        path: 'questions.id',
        model: Question,
      })
      .populate({
        path: 'chapter',
        model: Chapter,
      });
  } catch (err) {
    console.error(err);
    return err;
  }
};

const countCourses = async (skip, limit, sortField, sortOrder, filter) => {
  try {
    return await Course.find(filter)
      .sort({ [sortField]: sortOrder })
      .skip(skip)
      .limit(limit)
      .estimatedDocumentCount();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const findCourse = async filters => {
  try {
    return await Course
      .findOne(filters)
      .populate({
        path: 'questions.id',
        model: Question,
      })
      .populate({
        path: 'chapter',
        model: Chapter,
      });
  } catch (err) {
    return err;
  }
};

const addQuestion = async (id, questionId, order) => {
  const course = await Course.findById(id);

  if (!course) {
    throw new Error(`Impossible de trouver le cours #${id}`);
  }

  let { questions } = course;

  if (!questions) {
    questions = [];
  }

  const questionOrder = order ? order : questions.length + 1;

  questions.push({ id: questionId, order: questionOrder });

  course.questions = questions;

  try {
    questionRepository.updateQuestion(questionId, { courseId : course.id });
    return await course.save();
  } catch (err) {
    console.error(err);
    return err;
  }

};

export default {
  createCourse,
  getCourses,
  countCourses,
  findCourse,
  updateCourse,
  addQuestion
};
