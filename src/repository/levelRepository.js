import Level from '../models/level';
import Chapter from '../models/chapter';
import Language from '../models/language';
import chapterRepository from './chapterRepository';

const createLevel = async (input) => {
  const level = new Level({
    title: input.title,
    description: input.description,
  });

  try {
    return await level.save();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const getLevels = async (skip, limit, sortField, sortOrder, filter) => {
  try {
    return await Level.find(filter)
      .sort({ [sortField]: sortOrder })
      .skip(skip)
      .limit(limit)
      .populate({
        path: 'chapters.id',
        model: Chapter,
      })
      .populate({
        path: 'language',
        model: Language,
        populate: {
          path: 'levels.id',
          model: Level,
        }
      });
  } catch (err) {
    console.error(err);
    return err;
  }
};

const countLevels = async (skip, limit, sortField, sortOrder, filter) => {
  try {
    return await Level.find(filter)
      .sort({ [sortField]: sortOrder })
      .skip(skip)
      .limit(limit)
      .estimatedDocumentCount();
  } catch (err) {
    console.error(err);
    return err;
  }
};

const findLevel = async filters => {
  try {
    return await Level
      .findOne(filters)
      .populate({
        path: 'chapters.id',
        model: Chapter,
      })
      .populate({
        path: 'language',
        model: Language,
      });
  } catch (err) {
    return err;
  }
};

// todo : set chapter also
const addChapter = async (id, chapterId, order) => {
  const level = await Level.findById(id);

  if (!level) {
    throw new Error(`Impossible de trouver le niveau #${id}`);
  }

  let { chapters } = level;

  if (!chapters) {
    chapters = [];
  }

  const chapterOrder = order ? order : chapters.length + 1;

  chapters.push({ id: chapterId, order: chapterOrder });

  level.chapters = chapters;

  try {
    chapterRepository.updateChapter(chapterId, { levelId : level.id });

    return await level.save();
  } catch (err) {
    console.error(err);
    return err;
  }

};

export default {
  createLevel,
  getLevels,
  countLevels,
  findLevel,
  addChapter
};
