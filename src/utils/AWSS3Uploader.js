import AWS from "aws-sdk";
import dotenv from 'dotenv';
import { extname } from 'path';
import uniqid from 'uniqid';

dotenv.config({ path: '.env' });

const s3 = new AWS.S3({
  signatureVersion: 'v4',
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: "eu-west-3",
});

const uploadFile = (stream, filename, mimetype, type = 'image') => {

  const params = {
    Bucket: process.env.AWS_DESTINATION_BUCKET_NAME,
    Body: stream,               
    Key: `${type}/${uniqid()}${extname(filename)}`,  
    ContentType: mimetype 
  };

  // Uploading files to the bucket
  return s3.upload(params, (err, result) => {
    if (err) throw err;
    console.log(`File uploaded successfully. ${result.Location}`);
    return result.Location;
  }).promise();
};

export {
  uploadFile, 
};