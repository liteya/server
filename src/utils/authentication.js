export default () => context => {
  if (!context.user) {
    throw new AuthenticationError("Vous n'êtes pas connecté");
  }

  return context.user;
};
