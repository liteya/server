import express from 'express';
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import cors from 'cors';
import logger from 'morgan';
import jwt from 'express-jwt';
import "core-js/stable";
import "regenerator-runtime/runtime";
import { ApolloServer } from 'apollo-server-express';
import resolvers from './schemas/resolvers';
import typeDefs from './schemas/typeDefs';
import { userRepository, languageRepository, levelRepository, chapterRepository, courseRepository, questionRepository } from './repository/index'

dotenv.config({ path: '.env' });

const app = express();

/**
 * Connect to MongoDB.
 */
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.connect(process.env.MONGODB_URI, {
  useUnifiedTopology: true,
  useNewUrlParser: true
});
mongoose.connection.on('error', (err) => {
  console.error(err);
  console.log('%s MongoDB connection error. Please make sure MongoDB is running.');
  process.exit();
});

  try {
    logger.token('graphql-query', (req) => {
      if (req.body) {
        const { query, variables, operationName } = req.body;
        return `GRAPHQL: \nOperation Name: ${operationName} \nQuery: ${query} \nVariables: ${JSON.stringify(variables)}`;
      }
    });
  } catch (error) {
    console.error(error);
  }


app.use(cors(), logger(':graphql-query'), logger('dev'));
app.use(jwt({ secret: process.env.ACCESS_TOKEN_SECRET, algorithms: ['HS256'], credentialsRequired: false }));

const server = new ApolloServer({ 
  typeDefs, 
  resolvers,
  introspection: true,
  context: ({ req }) => ({
    user: req.user ? req.user : null,
    userRepository,
    languageRepository,
    levelRepository,
    courseRepository,
    chapterRepository,
    questionRepository
  }) 
});

server.applyMiddleware({ app });

app.listen({ port: process.env.PORT || 4000 });

console.log('Running a GraphQL API server at http://localhost:4000/graphql');