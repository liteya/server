import mongoose from 'mongoose';

const courseSchema = new mongoose.Schema({
  title: { type: String, required: true },
  description: { type: String },
  activated: {
    type: Boolean,
    default: false,
    required: true
  },
  chapter: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Chapter"
  },
  questions: [{
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Question"
    },
    order: { type: Number },
  }],
  //questions: [{
  //  index: { type: Number, required: true},
  //  type: { type: String, required: true },
  //  data: { type: String },
  //}]
}, { timestamps: true });

const Course = mongoose.model('Course', courseSchema);

export default Course;
