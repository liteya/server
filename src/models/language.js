import mongoose from 'mongoose';

const languageSchema = new mongoose.Schema({
  title: { type: String, required: true },
  description: { type: String },
  activated: {
    type: Boolean,
    default: false,
    required: true
  },
  levels: [{
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Level",
    },
    order: { type: Number },
  }]
}, { timestamps: true });

const Language = mongoose.model('Language', languageSchema);

export default Language;
