import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import validator from 'validator';

const premiumAccessFormulas = Object.freeze({
  BASIC: 'BASIC',
});

const userSchema = new mongoose.Schema({
  email: { 
    type: String, 
    unique: true, 
    required: true, 
    validate: {
      validator: v => validator.isEmail(v),
      message: props => `${props.value} n'est pas une adresse email valide!`
    }
  },
  password: { 
    type: String,
    validate: {
      // todo: Définir une règle plus complexe la validité d'un mot de passe
      validator: v => v.length >= 6,
      message: 'Le mot de passe doit avoir au moins 6 caractères'
    }
  },
  name: { type: String },
  premiumAccess: {
    activated: {
      type: Boolean,
      default: false,
      required: true
    },
    formula: {
      type: String,
      enum: Object.values(premiumAccessFormulas),
      required: false
    }
  },
  languages: [
    {
      id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Language",
      },
      isCurrentLearning: {
        type: Boolean,
        default: false,
        required: true
      },
      levels: [
        {
          id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Level",
          },
          chapters: {
            completed: [
              {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Chapter",
              }
            ],
            current: {
              id: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Chapter",
              },
              courses: {
                completed: [{
                  type: mongoose.Schema.Types.ObjectId,
                  ref: "Course",
                }]
              }
            }
          }
        }
      ]
    }
  ]
}, { timestamps: true });

/**
 * Password hash middleware.
 */
userSchema.pre('save', function save(next) {
  const user = this;
  if (!user.isModified('password')) { return next(); }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) { return next(err); }
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) { return next(err); }
      user.password = hash;
      next();
    });
  });
});

/**
 * Helper method for validating user's password.
 */
userSchema.methods.comparePassword = function comparePassword(candidatePassword) {
  return bcrypt
    .compare(candidatePassword, this.password)
    .then(res => res)
    .catch(err => {
        throw err;
    });
};

const User = mongoose.model('User', userSchema);

export default User;
