import mongoose from 'mongoose';

const chapterSchema = new mongoose.Schema({
  title: { type: String, required: true },
  description: { type: String },
  image: { type: Object },
  activated: {
    type: Boolean,
    default: false,
    required: true
  },
  premiumAccess: {
    type: Boolean,
    default: true,
    required: false
  },
  level: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Level"
  },
  courses: [{
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Course"
    },
    order: { type: Number },
  }]
}, { timestamps: true });

const Chapter = mongoose.model('Chapter', chapterSchema);

export default Chapter;
