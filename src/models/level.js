import mongoose from 'mongoose';

const levelSchema = new mongoose.Schema({
  title: { type: String, required: true },
  description: { type: String },
  activated: {
    type: Boolean,
    default: false,
    required: true
  },
  language: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Language"
  },
  chapters: [{
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Chapter"
    },
    order: { type: Number },
  }]
}, { timestamps: true });

const Level = mongoose.model('Level', levelSchema);

export default Level;
