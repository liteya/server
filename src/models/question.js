import mongoose from 'mongoose';

const questionSchema = new mongoose.Schema({
  title: { type: String },
  type: { type: String, required: true },
  content: { type: Object },
  activated: {
    type: Boolean,
    default: false,
    required: true
  },
  course: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Course"
  },
}, { timestamps: true });

const Question = mongoose.model('Question', questionSchema);

export default Question;
